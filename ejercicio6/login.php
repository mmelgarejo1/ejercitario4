<?php
    require '../../ejercitario4/ejercicio6/database.php';

    if(isset($_POST['submit'])&&!empty($_POST['submit'])) {
        $sql = "SELECT * FROM usuarios WHERE usuario = '". pg_escape_string($_POST['usuario']). "'";
        $result = pg_query($conn, $sql);
        $datos = pg_fetch_array($result);
        if($datos>0) {
            if(password_verify($_POST['password'], $datos['password'])) {
                echo "Bienvenido ". $datos['usuario'];
            } else {
            echo 'Usuario o contraseña invalidos';
            }
        } else {
            echo 'No hemos encontrado ningun resultado';
        }
    }

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../ejercitario4/ejercicio6/css/css/estilos.css" type="text/css">
    <title>Ingresar</title>
</head>
<body>
    <header>    
        <a href="../../ejercitario4/ejercicio6/">Inicio</a>
    </header>
    <form action="../../ejercitario4/ejercicio6/login.php" method="post">
        <h1> Ingresar </h1>
        <span> or <a href="../../ejercitario4/ejercicio6/signup.php"> Registrarse </a></span>
        <input type="text" name="usuario" placeholder="Ingresar usuario">
        <input type="password" name="password" placeholder="Ingresar contraseña">
        <input type="submit" name="submit" value="Ingresar">
    </form>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../ejercitario4/ejercicio6/css/css/estilos.css" type="text/css">
    <title>Bienvenido!</title>
</head>
<body>
    <header> 
        <a href="../../ejercitario4/ejercicio6/">Inicio</a>
    </header>
    <h1> Ingresar o Registrarse </h1>
    <a href="../../ejercitario4/ejercicio6/login.php"> Ingresar </a> o 
    <a href="../../ejercitario4/ejercicio6/signup.php"> Registrarse </a>
</body>
</html>

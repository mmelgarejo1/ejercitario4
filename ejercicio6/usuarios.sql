-- Table: public.usuarios

-- DROP TABLE public.usuarios;

CREATE TABLE IF NOT EXISTS public.usuarios
(
    id integer NOT NULL DEFAULT nextval('usuarios_id_seq'::regclass),
    usuario character varying(30) COLLATE pg_catalog."default",
    password character varying(200) COLLATE pg_catalog."default",
    CONSTRAINT usuarios_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.usuarios
    OWNER to postgres;
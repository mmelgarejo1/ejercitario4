<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../ejercitario4/ejercicio4/estilos.css" type="text/css">
    <title>Ejercicio 4</title>
</head>
<body>
    <?php
        require '../../ejercitario4/ejercicio4/database.php';

        $sql = $conn->query("SELECT p.nombre as NombreProducto, p.precio, m.nombre as NombreMarca, e.nombre NombreEmpresa, c.nombre as NombreCategoria FROM Producto p, Marca m, Categoria c, Empresa e WHERE p.id_marca = m.id_marca and m.id_empresa = e.id_empresa and p.id_categoria = c.id_categoria");
        $result = $sql->fetchAll(PDO::FETCH_OBJ);

        echo "<h1> Listado de Productos </h1>";

        echo "<table>";
            echo "<tr id='cabecera'>";
                echo "<th> Producto </th>";
                echo "<th> Precio </th>";
                echo "<th> Marca </th>";
                echo "<th> Empresa </th>";
                echo "<th> Categoria </th>";
            echo "</tr>";

            foreach($result as $resultado) {
                echo "<tr>";
                    echo "<td>". $resultado->nombreproducto . "</td>";
                    echo "<td>". $resultado->precio . "</td>";
                    echo "<td>". $resultado->nombremarca . "</td>";
                    echo "<td>". $resultado->nombreempresa . "</td>";
                    echo "<td>". $resultado->nombrecategoria . "</td>";
                echo "</tr>";
            }
        echo "</table>";
    ?>
</body>
</html>
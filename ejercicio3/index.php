<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../ejercitario4/ejercicio3/estilos.css" type="text/css">
    <title>Ejercicio 2</title>
</head>
<body>
    <?php
        require '../../ejercitario4/ejercicio3/database.php';

        $sql = "SELECT p.nombre as NombreProducto, p.precio, m.nombre as NombreMarca, e.nombre NombreEmpresa, c.nombre as NombreCategoria FROM Producto p, Marca m, Categoria c, Empresa e WHERE p.id_marca = m.id_marca and m.id_empresa = e.id_empresa and p.id_categoria = c.id_categoria";

        $ret = pg_query($conn, $sql);

        echo "<h1> Listado de Productos </h1>";

        if(pg_num_rows($ret) > 0) {
            echo "<table>";
                echo "<tr id='cabecera'>";
                    echo "<th> Producto </th>";
                    echo "<th> Precio </th>";
                    echo "<th> Marca </th>";
                    echo "<th> Empresa </th>";
                    echo "<th> Categoria </th>";
                echo "</tr>";
                while($data = pg_fetch_array($ret)) {
                    echo "<tr>";
                        echo "<td>". $data['nombreproducto'] . "</td>";
                        echo "<td>". $data['precio'] . "</td>";
                        echo "<td>". $data['nombremarca'] . "</td>";
                        echo "<td>". $data['nombreempresa'] . "</td>";
                        echo "<td>". $data['nombrecategoria'] . "</td>";
                    echo "</tr>";
                }    
            echo "</table>";
        } else {
            echo "<h1> No se encontraron productos para mostrar";
        }

    ?>
</body>
</html>
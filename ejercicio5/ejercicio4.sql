-- This script was generated by a beta version of the ERD tool in pgAdmin 4.
-- Please log an issue at https://redmine.postgresql.org/projects/pgadmin4/issues/new if you find any bugs, including reproduction steps.
BEGIN;


CREATE TABLE IF NOT EXISTS public.ejercicio4
(
    id serial NOT NULL,
    nombre character varying(40),
    descripcion character varying(250),
    PRIMARY KEY (id)
);
END;
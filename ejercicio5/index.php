<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 5</title>
</head>
<body>
    <?php
        include '../../ejercitario4/ejercicio5/database.php';

        function generateRandomString() {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < 10; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        } 
        $i=0;
        while($i<1000) {
            $sql = "INSERT INTO ejercicio4(nombre, descripcion) VALUES('".generateRandomString()."','".generateRandomString()."')";
            $ret = pg_query($conn, $sql);
            $i++;
        }
            
        if($ret){
            echo "Insertado correctamente";
        }else{
            echo "Algo salio mal. Intente nuevamente";
        } 
    ?>
</body>
</html>